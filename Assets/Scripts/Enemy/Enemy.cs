﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    public float speed;
    public float attackTime;
    private float currentTime=0;
    public GameObject BulletPrefabEnemy;
    public AudioSource audioEnemyLaser;
    

    Transform graphics;

    public ParticleSystem ps;

    public AudioSource audioSource;

    // Start is called before the first frame update
    void Awake()
    {
         graphics = transform.GetChild(0);

        for (int i=0; i<graphics.childCount; i++){

            graphics.GetChild(i).gameObject.SetActive(false);
        }


            int seleccionado = Random.Range(0,graphics.childCount);

            graphics.GetChild(seleccionado).gameObject.SetActive(true);      

    }
    void Update(){

        transform.Translate (speed * Time.deltaTime,0, 0);  

        currentTime+=Time.deltaTime;

        if(currentTime>=attackTime){


           speed=0;

            if(currentTime>=2*attackTime){

                speed=-3;
                EnemyShoot();
                currentTime=0;
                
            }       
        }  
    }

    public void EnemyShoot(){
        Instantiate (BulletPrefabEnemy, this.transform.position, Quaternion.identity, null);
        audioEnemyLaser.Play();
    }

    public void OnTriggerEnter2D(Collider2D other){
        Debug.Log("Soy trigger");
        if (other.tag == "Finish"){
            Destroy(this.gameObject);
        }
        
        else if(other.tag == "Bullet") {
            StartCoroutine(DestroyEnemy());
        }

   }

        IEnumerator DestroyEnemy(){

    graphics.gameObject.SetActive(false);

    Destroy(GetComponent<BoxCollider2D>());

    ps.Play();

    audioSource.Play();


    yield return new WaitForSeconds(1.0f);

    Destroy(this.gameObject);
}


}