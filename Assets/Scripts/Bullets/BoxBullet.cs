﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class BoxBullet : MonoBehaviour
{
    public float speed;
    public float timeBox;
 
    private float currentTimeBox=0;
 
   
    // Update is called once per frame
    void Update () {
        currentTimeBox += Time.deltaTime;

        if(currentTimeBox< timeBox*1 && currentTimeBox < timeBox * 2)
        {
            transform.Translate (speed * Time.deltaTime,0, 0);
        }

        else if (currentTimeBox > timeBox * 2 && currentTimeBox < timeBox * 3)
        {
            transform.Translate(0, -speed * Time.deltaTime, 0);
        }

        else if (currentTimeBox > timeBox * 3 && currentTimeBox < timeBox * 4)
        {
            transform.Translate(-speed * Time.deltaTime, 0, 0);
        }

        else if(currentTimeBox>timeBox * 4 && currentTimeBox<timeBox*6){
            transform.Translate (0, speed * Time.deltaTime,0);        
        }

        

        else 
        {
            transform.Translate(speed * Time.deltaTime, 0, 0);
        }
        
    }
 
    public void OnTriggerEnter2D(Collider2D other){
        Debug.Log("Soy trigger");
        if (other.tag == "Finish" || other.tag == "Meteor" || other.tag == "Enemy") {
            Destroy (gameObject);
            currentTimeBox = 0;
        }
    }
 
}