﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletVertical : MonoBehaviour
{
    public float speed;

    
    // Update is called once per frame
    void Update () {
        transform.Translate (0,speed * Time.deltaTime,0);
    }

    public void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "Finish" || other.tag == "Meteor" || other.tag == "Enemy") {
            Destroy (gameObject);
        }
    }

}
