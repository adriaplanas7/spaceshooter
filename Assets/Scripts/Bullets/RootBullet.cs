﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RootBullet : MonoBehaviour
{
    public float speed;

    public float timeRoot;
 
    private float currentTimeRoot=0;

    public GameObject BulletRoot;
    
    // Update is called once per frame
    void Update(){
    
    transform.Translate (speed * Time.deltaTime,0,0);

    currentTimeRoot += Time.deltaTime;
    
    if(currentTimeRoot>timeRoot){   
    Instantiate (BulletRoot, this.transform.position, Quaternion.identity, null);
    GameObject go = Instantiate (BulletRoot, this.transform.position, Quaternion.identity, null);
    go.transform.Rotate(0,0,30);
    GameObject go2 = Instantiate (BulletRoot, this.transform.position, Quaternion.identity, null);
    go2.transform.Rotate(0,0,-30);

    currentTimeRoot=0;
    }
    }
    public void OnTriggerEnter2D(Collider2D other){
        if (other.tag == "Finish" || other.tag == "Meteor" || other.tag == "Enemy") {
            Destroy (gameObject);
        }
    }
}

    

