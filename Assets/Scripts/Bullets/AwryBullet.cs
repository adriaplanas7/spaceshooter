﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class AwryBullet : MonoBehaviour
{
    public float speed;
    public float timeAwry;
 
    private float currentTimeAwry=0;
 
   
    // Update is called once per frame
    void Update () {
        currentTimeAwry += Time.deltaTime;
        if(currentTimeAwry<timeAwry){
            transform.Translate (speed * Time.deltaTime,0, 0);
        }else if(currentTimeAwry>timeAwry && currentTimeAwry<timeAwry*2){
            transform.Translate (0, speed * Time.deltaTime,0);        
        }else if(currentTimeAwry>timeAwry*2 && currentTimeAwry<timeAwry*3){
            transform.Translate (speed * Time.deltaTime,0, 0);
        }else if(currentTimeAwry>timeAwry*3 && currentTimeAwry<timeAwry*5){
            transform.Translate (0, -speed * Time.deltaTime,0);
        }else if(currentTimeAwry>timeAwry*5 && currentTimeAwry<timeAwry*6){
            transform.Translate (speed * Time.deltaTime,0, 0);
        }else if(currentTimeAwry>timeAwry*6 && currentTimeAwry<timeAwry*7){
            transform.Translate (0, speed * Time.deltaTime,0);
        }else{
            currentTimeAwry = 0;
        }
    }
 
    public void OnTriggerEnter2D(Collider2D other){
        Debug.Log("Soy trigger");
        if (other.tag == "Finish" || other.tag == "Meteor" || other.tag == "Enemy") {
            Destroy (gameObject);
        }
    }
 
}