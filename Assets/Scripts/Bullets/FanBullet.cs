﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FanBullet : MonoBehaviour
{
    public float speed;

    public float timeFan;

    private float currentTimeFan = 0;

    public GameObject BulletFan;

    // Update is called once per frame
    void Update()
    {

        transform.Translate(speed * Time.deltaTime, 0, 0);

        currentTimeFan += Time.deltaTime;

        if (currentTimeFan > timeFan)
        {
            Instantiate(BulletFan, this.transform.position, Quaternion.identity, null);
            GameObject go = Instantiate(BulletFan, this.transform.position, Quaternion.identity, null);
            go.transform.Rotate(0, 0, 45);
            GameObject go2 = Instantiate(BulletFan, this.transform.position, Quaternion.identity, null);
            go2.transform.Rotate(0, 0, -45);
            GameObject go3 = Instantiate(BulletFan, this.transform.position, Quaternion.identity, null);
            go.transform.Rotate(0, 0, 90);
            GameObject go4 = Instantiate(BulletFan, this.transform.position, Quaternion.identity, null);
            go2.transform.Rotate(0, 0, -90);
            GameObject go5 = Instantiate(BulletFan, this.transform.position, Quaternion.identity, null);
            go.transform.Rotate(0, 0, 135);
            GameObject go6 = Instantiate(BulletFan, this.transform.position, Quaternion.identity, null);
            go.transform.Rotate(0, 0, -135);




            currentTimeFan = 0;
        }
    }
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Finish" || other.tag == "Meteor" || other.tag == "Enemy") 
        {
            Destroy(gameObject);
            currentTimeFan = 0;
        }
    }
}