﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeWeapon : Weapon
{

    public GameObject TimeBullet;
    public float cadencia;

    public override float GetCadencia()
    {
        return cadencia;
    }

    public override void Shoot()
    {
        Instantiate (TimeBullet, this.transform.position, Quaternion.identity, null);
    }
}
