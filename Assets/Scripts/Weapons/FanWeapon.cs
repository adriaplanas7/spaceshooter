﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class FanWeapon : Weapon
{
 

    public GameObject Bullet;

        public float cadencia;

    
 
    
    //currentTimeRoot += Time.deltaTime;
 
    public override float GetCadencia()
    {
        return cadencia;
    }
 
    public override void Shoot()
    {
        Instantiate (Bullet, this.transform.position, Quaternion.identity, null);
       
    }
}