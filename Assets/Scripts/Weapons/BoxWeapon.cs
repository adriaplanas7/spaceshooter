﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxWeapon : Weapon
{

    public GameObject BoxBullet;
    


    public float cadencia;

    public override float GetCadencia()
    {
        return cadencia;
    }

    public override void Shoot()
    {
        Instantiate (BoxBullet, this.transform.position, Quaternion.identity, null);
        
    }
}
