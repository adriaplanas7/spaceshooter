﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AwryWeapon : Weapon
{

    public GameObject AwryBullet;
    


    public float cadencia;

    public override float GetCadencia()
    {
        return cadencia;
    }

    public override void Shoot()
    {
        Instantiate (AwryBullet, this.transform.position, Quaternion.identity, null);
        
    }
}
